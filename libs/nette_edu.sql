-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `weight` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `inmenu` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `categories` (`id`, `title`, `weight`, `inmenu`) VALUES
(1,	'Languages',	'0',	0),
(2,	'Science',	'0',	0),
(3,	'Art',	'0',	0),
(4,	'Sport',	'0',	0),
(5,	'History',	'0',	0),
(6,	'Social',	'0',	0),
(7,	'Religion',	'0',	0),
(8,	'IT',	'0',	0),
(9,	'Crafts',	'0',	0),
(10,	'Astrology',	'0',	0),
(11,	'Busyness',	'0',	0),
(12,	'Communication',	'0',	0),
(13,	'Politics',	'0',	0);

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `created` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `inCategory` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `category_id` int(10) unsigned NOT NULL,
  `inmenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_category` (`category_id`),
  KEY `order` (`category_id`,`inCategory`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `class` (`id`, `name`, `content`, `created`, `inCategory`, `category_id`, `inmenu`) VALUES
(1,	'English subject',	'english class test content ',	'2015-05-02 12:08:50',	1,	1,	0),
(2,	'Math',	'math class test content',	'0000-00-00 00:00:00',	1,	2,	0),
(3,	'Oil Paintings',	'Oil Paintings test content',	'0000-00-00 00:00:00',	1,	3,	0),
(4,	'Tenis',	'Tenis class test content',	'0000-00-00 00:00:00',	1,	4,	0),
(5,	'Middle age in europe',	'Middle age in europe class test content',	'0000-00-00 00:00:00',	1,	5,	0),
(6,	'Business law',	'Business law class test content',	'0000-00-00 00:00:00',	1,	6,	0),
(9,	'neco',	'neco',	'0000-00-00 00:00:00',	1,	0,	0);

DROP TABLE IF EXISTS `curricula`;
CREATE TABLE `curricula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`class_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `node_id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order` (`node_id`,`class_id`,`status`,`created`),
  KEY `fk_class` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `node`;
CREATE TABLE `node` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `prefix` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `node` (`id`, `user_id`, `name`, `title`, `prefix`, `content`, `created`) VALUES
(1,	0,	'Homepage',	'Home',	'Educatis - more then education!',	'Educatis homepage default text.',	'2015-04-16 12:18:52'),
(2,	0,	'Classes',	'Classes',	'Join the class and advertise the experiences',	'Classes default content.',	'2015-04-16 12:20:21'),
(3,	0,	'Contact',	'Contact',	'Contact the webmaster',	'Contact form page.',	'2015-04-16 12:21:03'),
(5,	0,	'',	'Tools',	'',	'Tools startup page',	'2015-04-16 14:50:51');

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `profile` (`id`, `type`) VALUES
(1,	'Pupil'),
(2,	'Student'),
(3,	'Class owner');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` char(60) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `role` varchar(30) COLLATE utf8_czech_ci NOT NULL DEFAULT 'user',
  `active` char(1) COLLATE utf8_czech_ci NOT NULL DEFAULT '1',
  `name` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  `avatar` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  `change_email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`, `active`, `name`, `avatar`, `change_email`) VALUES
(1,	'architect',	'$2y$10$K.JEAIhI/bmk2Kas2uxFi.3Y.qJ6LZNw44X5k9Lq81R27wgcZqsSu',	'info@aprila.cz',	'root',	'1',	'Architect',	'',	''),
(2,	'marekteresak0201@gmail.com',	'$2y$10$2M8MHpqj6BFaX3.snGCiV.nNtPb8FeAbRk2IZRZJv9rR1mKL4bZh.',	'Marek',	'user',	'1',	'medovyledik',	'',	''),
(3,	'bavorak@bavorak.cz',	'$2y$10$6leD7SGWhI548DqGdBzfLepe3m/ULly1VGtvY4h5js0ZbOtjo.P9m',	'luky',	'user',	'1',	'testtest',	'',	''),
(6,	'borek@borek.vz',	'$2y$10$L8az.jq.MI684r7TevylIOv0PRBmcVlyDdoJcSqF2SQ14dh0cvfZW',	'tele',	'user',	'1',	'testtesttest',	'',	''),
(8,	'marekteresak@gmail.com',	'$2y$10$Os3VnalQa2q8G9spLQI5NO0CUKgUmSPF8EdmXqg0lHKdRptq188q2',	'marekteresak@gmail.com',	'user',	'1',	'marekteresak@gmail.com',	'',	'');

-- 2015-05-22 11:14:52
