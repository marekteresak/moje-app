<?php

namespace System\Forms;

use Nette,
	Model,
	System,
	Render\Forms,
	Render\Utils,
	System\Render\DuplicateEntryException,
	Nette\Application\UI,
	Nette\Security\User;


class UserFormFactory extends Nette\Object
{
	/** @var User */
	protected $user;

	/** @var Model\Authenticator  */
	protected $authenticator;


	public function __construct(Nette\Security\User $user, Model\Authenticator $authenticator)
	{
		$this->user = $user;
		$this->authenticator = $authenticator;
	}


	/**
	 * @return UI\Form
	 */
	public function createRegistrationForm()
	{
		$form = new UI\Form;

		$form->addText('email', 'Username:');

		$form->addText('liame', 'Email:')
			->setType('email')
			->setRequired('Please enter e-mail.')
			->addRule(UI\Form::EMAIL, 'This is not valid e-mail.');

		$form->addText('name', 'Name:')
			->setRequired('Please enter your name.');

		$form->addPassword('password', 'Password:')
			->setRequired('Please enter your password.')
			->addRule(UI\Form::MIN_LENGTH, 'Password min length is %d chars', 6); //


		$form->addSubmit('send', 'Sign in');

		$form->onSuccess[] = array($this, 'registrationFormSucceeded');
		return $form;
	}


	public function registrationFormSucceeded($form, $values)
	{
		$this->user->setExpiration('14 days', FALSE);

		try {
			$this->authenticator->addUser($values->liame, $values->liame, $values->password, 'user', $values->name);
		} catch (DuplicateEntryException $e){
			$form['liame']->addError('User with this email is registered. Please sign in');

			return true;
		}

		try {
			$this->user->login($values->liame, $values->password);
		} catch (Nette\Security\AuthenticationException $e) {

			$form->addError($e->getMessage());
		}
	}
}
