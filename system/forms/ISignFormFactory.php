<?php

namespace System\Forms;

interface ISignFormFactory
{

	/**
	 * @return SignForm
	 */
	function create();

}
