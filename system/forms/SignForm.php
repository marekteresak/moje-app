<?php

namespace System\Forms;

use Nette,
	System,
	Model,
	Nette\Forms,
	Nette\Security,
    Render,
    Nette\Application\UI;

class SignForm extends Nette\Application\UI\Control
{
	/** @var array */
	public $onSuccess;

    /** @var Nette\Security\User */
    private $user;

    /** @var Nette\Http\Session */
    private $session;

    /** @var int */
    public $loginCounter = 0;

    public function __construct(Nette\Security\User $user, Nette\Http\Session $session)
    {
        $this->user = $user;
        $this->session = $session;
    }

    /**
     * @param UI\Form $form
     */
    public function processForm(UI\Form $form)
    {
        $values = $form->getValues();
        $login = $this->session->getSection('login');

        if (isset($login->counter) && $login->counter > 5) {
            sleep(2);
        }

        if ($values->remember) {
            $this->user->setExpiration('14 days', FALSE);
        } else {
            $this->user->setExpiration('20 minutes', TRUE);
        }

        try {
            $this->user->login($values->username, $values->password);

            $login->counter = 0;

			$this->onSuccess();

        } catch (Nette\Security\AuthenticationException $e) {

            $login->counter++;

            $form->addError($e->getMessage());
        }


    }

    /**
     * @return UI\Form
     */
    protected function createComponentForm()
    {
        $form = new UI\Form;
        $form->addText('username', 'Username:')
            ->setRequired('Please enter your username.');

        $form->addPassword('password', 'Password:')
            ->setRequired('Please enter your password.');

        // TODO : simple captcha system?
        // if ($login->counter > 5)
        // $form->addText('captcha', 'Are You human?');

        $form->addCheckbox('remember', 'Keep me signed in');

        $form->addSubmit('send', 'Log in');

		$form->onSuccess[] = $this->processForm;

        return $form;
    }

}
