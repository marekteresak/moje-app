<?php
/**
 * Created by PhpStorm.
 * User: Madrek
 * Date: 25.5.2015
 * Time: 22:09
 */
namespace System\Forms;

use Nette,
	Model,
	System,
	Render\Forms,
	Render\Utils,
	System\Render\DuplicateEntryException,
	Nette\Application\UI,
	Nette\Application\UI\Form,
	Nette\Security\User;

class PostFormFactory extends Nette\Object
{
	/** @var Nette\Security\User */
	protected $user;

	/** @var Model\Authenticator  */
	protected $authenticator;


	public function __construct(Nette\Security\User $user, Model\Authenticator $authenticator)
	{
		$this->user = $user;
		$this->authenticator = $authenticator;
	}

	/**
	 * @return UI\Form
	 */
	public function createComponentPostForm()
	{
		$form = new UI\Form;
		$form->addText('name', 'Name:');
		$form->addText('title', 'Title:')
			->setRequired();
		$form->addTextArea('content', 'Add post:')
			->setRequired();
		$form->addSubmit('send', 'Public new content');
		$form->onSuccess[] = array($this, 'postFormSucceeded');
		return $form;
	}

	/**
	 * @return UI\Form
	 */
	public function postFormSucceeded($form, $values)
	{
		$values = $this->getValues('classId');
		if ($classId) {
			$class = $this->database->table('class')->get($classId);
			$class = $this->table->update($values);
		} else { $class = $this->database->table('class')->insert($values);
		}
		$this->flashMessage('Content was published', 'success');
		$this->redirect('show', $class->id);
	}
}
