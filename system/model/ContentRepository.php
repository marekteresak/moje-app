<?php

namespace Model;

use Nette;

class ContentRepository extends Repository
{

	public function findById($nodeId)
	{
		return $this->table('node')->where(array('id' => $nodeId))->fetch();
	}

	public function findUserAtNode()
	{
		return $this->findById(array('user_id' => true))->order('created  ASC');
	}

	/**
	 * @int $userId
	 * @int $nodeId
	 * @string $node
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function createNodeByUser($node, $userId, $nodeId)
	{
		$userId = Nette\Security\User::getId($userId);

		$data = array(
			'content' => $node,
			'user_id' => $userId,
			'created' => new \DateTime(),
			'id' => $nodeId,
		);
		while ($this->table('node')->where(array('user_id' => $userId))->fetch()) {
			$person = $this->table('users')->where(array('id' => true))->insert($userId);
		}
		if($data !== false || $this->getCount()){
			$data = $this->findFulltext($data)->where(array(
				'%' . $userId . '%',
				'%' . $node . '%',
				))->createSelectionInstance('users', 'node' == true);
			}
	}

	public function getPostValues($values)
	{
		return $this->table('node')->update(array(
			'name' => $values['name'],
			'title' => $values['title'],
			'content' => $values['content'],
			'created' => new \DateTime(),
		));
	}
}
