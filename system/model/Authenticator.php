<?php

namespace Model;

use Nette,
	System,
	Nette\Security,
	Nette\Utils\ArrayHash,
	System\Render\DuplicateEntryException,
	Nette\Security\Passwords,
	System\Render\InvalidArgumentException;

class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator
{
	/**
	 * @var Nette\Database\Context
	 */
	protected $database;

	/*
		Educatis base user roles:
		- guest
		- student
		- teacher
		- root
	*/

	/**
	* User roles
	*
	* @var array
	*/
	protected $roles = array(
		"user" => "student",
		"admin" => "teacher",
		"root" => "super user"
	);

	/**
	 * @param Nette\Database\Context $database
	 */
	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	/**
	 * Performs an authentication
	 *
	 * @param array $credentials
	 * @return Security\Identity|Security\IIdentity
	 * @throws Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		$row = $this->database->table('users')
			->where('username', $username)
			->where('active', '1')
			->fetch();

		if (!$row) {
			throw new Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);

		} elseif (!Passwords::verify($password, $row->password)) {
			throw new Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

		} elseif (Passwords::needsRehash($row->password)) {
			$row->update(array(
				'password' => Passwords::hash($password),
			));
		}

		$arr = $row->toArray();
		unset($arr['password']);

		return new Security\Identity($row->id, $row->role, $arr);
	}


	/**
	 * Computes salted token hash.
	 *
	 * @param $token
	 * @param $salt
	 * @return string
	 */
	public static function calculateHash($token, $salt)
	{
		return hash('sha256', $token . $salt);
	}


	/**
	 * Generates salt.
	 *
	 * @param int $length
	 * @return string
	 */
	public static function generateToken($length = 32)
	{
		return Random::generate($length, '0-9a-z');
	}

/*
	public function registerUser($name, $nick, $email, $note, $password)
	{
		$this($name, $nick, $email, $note, $password)
		$row = $this->user->findByName($name);
	}*/

	/**
	 * Return user by Id (ignored status for deactivation)
	 *
	 * @param $id
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function get($id)
	{
		return $this->database->table('users')->get($id);
	}

	/**
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function findByName($name)
	{
		return $this->findBy(array('name' => $name))->fetch();
	}


	public function getActiveUser($id)
	{
		return $this->database->table('users')
					->where('id', $id)
					->where('active', '1')
					->fetch();
	}

	public function findAll()
	{
		return $this->database->table('users')
					->where('active', '1')
					->fetchAll();
	}

	/**
	 * Vrací řádky podle query
	 *
	 * @param string search query
	 * @return Array
	 */
	public function findFulltext($query)
	{
		$like = '%' . $query . '%';

		return $this->database->table('users')
			->where('username LIKE ?', $like)
			->where('active', '1')
			->fetchAll();
	}

	/**
	 * Return all deactivated users
	 *
	 * @return array|Nette\Database\Table\IRow[]
	 */
	public function findAllDeactivated()
	{
		return $this->database->table('users')
			->where('active', '0')
			->fetchAll();
	}

	/**
	 * Inserts new user
	 * @param array|ArrayHash $form
	 * @return Nette\Database\Table\ActiveRow
	 * @throws DuplicateEntryException
	 * @throws \PDOException
	 */
	public function addUser($email, $username, $password, $role, $name)
	{
		$password = Passwords::hash($password);

		$data = array(
			'email' => $email,
			'role' => $role,
			'username' => $username,
			'password' => $password,
			'name' => $name
		);

		try {
			$person = $this->database->table('users')->insert($data);

		} catch (\PDOException $e) {
			if ($e->getCode() == '23000') {
				throw new DuplicateEntryException;
			} else {
				throw $e;
			}
		}

		return $person;

	}

	/**
	 * Inserts new user
	 * todo: use it or add()?
	 * @param array|ArrayHash $form
	 * @return Nette\Database\Table\ActiveRow
	 * @throws DuplicateEntryException
	 * @throws \PDOException
	 */
	public function add($form)
	{
		$password = Passwords::hash($form['password']);

		$data = array(
			'email' => $form['email'],
			'role' => $form['role'],
			'username' => $form['username'],
			'password' => $form['password'],
			'name' => isset($form->name) ? $form->name : ''
		);

		try {
			$person = $this->database->table('users')->insert($data);

		} catch (\PDOException $e) {
			if ($e->getCode() == '23000') {
				throw new DuplicateEntryException;
			} else {
				throw $e;
			}
		}

		return $person;

	}

	/**
	 * Deactivate user
	 *
	 * @param $userId
	 * @return bool
	 */
	public function deactivateUser($userId)
	{
		$user = $this->get($userId);
		if ($user) {
			$user->update(array('active' => '0'));

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * @param $password string
	 * @param $new_password string
	 * @param $userId
	 * @return bool
	 * @throws InvalidArgumentException
	 */
	public function changeUserPassword($password, $new_password, $userId)
	{
		$user = $this->database->table('users')
			->where('id', $userId)
			->fetch();

		if (!Passwords::verify($password, $user->password)) {
			throw new DuplicateEntryException('The origin password is incorrect');

		} else {
			$this->database->table('users')
				->where('id', $userId)
				->update(array('password' => Passwords::hash($new_password)));

			return TRUE;
		}
	}


	/**
	 * @param $password
	 * @param $userId
	 * @return bool
	 */
	public function setUserPassword($password, $userId)
	{
		$user = $this->database->table('users')
			->where('id', $userId);

		if ($user) {
			$user->update(array('password' => Passwords::hash($password)));

			return TRUE;
		}

		return FALSE;
	}


	/**
	 * @param $userId int
	 * @return bool
	 */
	public function activateUser($userId)
	{
		$user = $this->get($userId);
		if ($user) {
			$user->update(array('active' => '1'));

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * @param $userId int
	 * @param $change array
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function edit($userId, $change)
	{
		$data = array();

		if (isset($change['name'])) {
			$data['name'] = $change['name'];
		}

		$avatar = NULL;

		if ($this->useFiles && isset($change['avatar'])) {
			$avatar = $change['avatar'];

			/** @noinspection PhpUndefinedMethodInspection */
			if ($avatar->isOk()) {
				/** @noinspection PhpUndefinedMethodInspection */
				$data['avatar'] = $this->saveImage($avatar);
			}
		}

		$this->get($userId)->update($data);

		return $this->get($userId);
	}

	public function findUserRoles()
	{
		return $this->roles;
	}
}
