<?php

namespace Model;

use Nette,
	Nette\Database\Context,
	System\Render\DuplicateEntryException,
	System;


abstract class Repository extends Nette\Object
{
	/** @var \Nette\Database\Context */
	private $database;

	/** @var string */
	public $table;

	/**
	* @param \Nette\Database\Context $database
	*/
	public function __construct(Context $database)
	{
		$this->database = $database;
	}

	/**
	* @return Nette\Database\Table\Selection
	*/
	public function table()
	{
		return $this->database->table($this->table);
	}

	/**
	* @param $id
	* @return Nette\Database\Table\IRow
	*/
	public function get($id)
	{
		return $this->table()->get($id);
	}

	/**
	* @return int
	*/
	public function getCount()
	{
		return $this->table()->count();
	}

	/**
	 * @return Nette\Database\Table\Selection
	 */
	public function findAll()
	{
		return $this->table();
	}

	/**
	* @return Nette\Database\Table\Selection
	*/
	public function findBy(array $by)
	{
		return $this->table()->where($by);
	}

	/**
	* @param $query
	* @return Nette\Database\Table\Selection
	*/
	public function findFulltext($query)
	{
		return $this->table()
			->where('title LIKE ? OR content LIKE ?',
				array(
					"%" . $query . "%",
					"%" . $query . "%")
			);
	}

	/**
	* @param $data
	* @param $id
	* @throws DuplicateEntryException
	* @return Nette\Database\Table\IRow
	*/
	public function insert($id, $data)
	{
		try {
			$newRow = $this->table()->insert($data);
		} catch (\PDOException $e) {
			if ($e->getCode() == '23000') {
				throw new DuplicateEntryException;
			} else {
				throw $e;
			}
		}
		return $this->get($id);
	}

	/**
	* @param $data
	* @param $id
	* @throws DuplicateEntryException
	* @return Nette\Database\Table\IRow
	*/
	public function update($id, $data)
	{
		try {
			/** @noinspection PhpUndefinedMethodInspection */
			$this->get($id)->update($data);
		} catch (\PDOException $e) {
			if ($e->getCode() == '23000') {
				throw new DuplicateEntryException;
			} else {
				throw $e;
			}
		}
		return $this->get($id);
	}

	/**
	* @param $id int
	* @return bool
	*/
	public function hardDelete($id)
	{
		/** @noinspection PhpUndefinedMethodInspection */
		$this->get($id)->delete();

		return TRUE;
	}

	/**
	* @param $id
	* @return bool
	*/
	public function delete($id)
	{
		return $this->hardDelete($id);
	}
}
