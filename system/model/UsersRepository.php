<?php

namespace Model;

use Nette;

class UsersRepository extends Repository
{
	public function findAll()
	{
		return $this->table('users')->fetch();
	}

	public function findByName($name)
	{
		return $this->table('users')->where(array('name' => $name))->fetch();
	}

	public function findByUserId($userId)
	{
		return $this->table('users')->where(array('id' => $userId))->fetch();
	}

	public function getCount()
	{
		return $this->table('users')->count(array('active' => TRUE));
	}

}
