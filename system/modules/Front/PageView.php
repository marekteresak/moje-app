<?php
/**
 * Created by PhpStorm.
 * User: Marek
 * Date: 21.5.15
 * Time: 2:07
 */
namespace System\Module\Front\Views;

use Nette,
	Nette\Database,
	Model,
	System;

class PageView extends System\BaseView
{
	/** @var Model\UsersRepository */
	public $users;

	/** @var Model\ContentRepository */
	public $content;

	public function __construct(Model\UsersRepository $users, Model\ContentRepository $content)
	{
		$this->users = $users;
		$this->content = $content;
	}

	public function beforeRender()
	{
		if($this->getPresenter()->isAjax()){
			if($this->getUser()->isLoggedIn()){
				$this->redirect('Posts:content');
			} if(!$this->getUser()->isLoggedIn()) {
				$this->redirect('Sign:default');
			} else if($this->getUser()->onLoggedIn){
				$this->user->getIdentity();
			} else{
				$this->getParent()->addComponent();
			}
		}
	}


}
