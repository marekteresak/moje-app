<?php
/**
 * Created by PhpStorm.
 * User: Marek
 * Date: 21.5.15
 * Time: 2:14
 */

namespace System\Module\Front\Views;

use Nette,
	System\Forms,
	Model,
	Nette\Application\UI,
	Nette\Security,
	System;

class SignView extends System\BaseView
{
	/** @var Forms\ISignFormFactory */
	private $signFormFactory;

	/** @var Forms\UserFormFactory */
	private $formFactory;

	/** @var int */
	public $loginCounter = 0;

	public function __construct(Forms\ISignFormFactory $signFormFactory, Forms\UserFormFactory $formFactory)
	{
		parent::__construct();
		$this->signFormFactory = $signFormFactory;
		$this->formFactory = $formFactory;
	}

	public function render()
	{
		echo $this['form'];
	}

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('You have been signed out.');
		$this->redirect('Sign:');
	}


	public function renderIn()
	{
		$login = $this->session->getSection('login');
		if (isset($login->counter)) {
			$this->loginCounter = $login->counter;
		}
		$this->template->loginCounter = $this->loginCounter;
	}

	/**
	 * Sign-in form factory.
	 *
	 * @return UI\Form
	 */
	protected function createComponentSignForm()
	{
		$signForm = $this->signFormFactory->create();

		$signForm->onSuccess[] = function () {
			$this->redirect('Posts:content');
		};

		return $signForm;

	}

	/**
	 * Sign-up Form Factory.
	 *
	 * @return UI\Form
	 */
	protected function createComponentRegistrationForm()
	{
		$registrationForm = $this->formFactory->createRegistrationForm();

		$registrationForm->onSuccess[] = function () {
			$this->flashMessage('You are successfully registered and logged in');
			$this->redirect('Posts:content');
		};

		return $registrationForm;

	}

}
