<?php
/**
 * Created by PhpStorm.
 * User: Marek
 * Date: 21.5.15
 * Time: 0:41
 */
namespace System\Module\Front\Views;

use Nette,
	Model\Authenticator,
	Nette\Application\UI,
	Nette\Database,
	System;

class UsersView extends System\BaseView
{
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function renderUsers()
	{
		$this->template->users = $this->database->table('users');
	}

	public function renderShow($usersId)
	{
		$this->template->users = $this->database->table('users')->get($usersId);
	}
}


