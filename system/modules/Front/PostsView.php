<?php
/**
 * Created by PhpStorm.
 * User: Marek
 * Date: 21.5.15
 * Time: 1:59
 */
namespace System\Module\Front\Views;

use Nette,
	Model,
	Nette\Application,
	Nette\Database,
	Nette\Application\UI,
	Nette\Application\UI\Form,
	Nette\Security\User,
	System;

class PostsView extends System\BaseView
{
	/** @return Nette\Database\Context */
	private $database;

	/** @var Model\ContentRepository */
	public $content;

	/** @var Model\UsersRepository */
	public $users;

	public function __construct(Nette\Database\Context $database, Model\ContentRepository $content, Model\UsersRepository $users)
	{
		$this->database = $database;
		$this->content = $content;
		$this->users = $users;
	}

	public function actionShow($nodeId)
	{
		$this->template->node = $this->database->table('node')->get($nodeId);
		if($nodeId >= 1){
			$this->presenter->isAjax();
			$this->getPresenter('this')->formatActionMethod('show');
		} else if($nodeId == false){
			$this->redirect('content');
		}
	}

	public function actionContent()
	{

		$this->template->node = $this->database->table('node')
					->order('created ASC');
	}

	/**
	 * @return UI\Form
	 */
	public function createComponentPostForm()
	{
		$form = new UI\Form;
		$form->addText('name', 'Name:');
		$form->addText('title', 'Title:')
			->setRequired();
		$form->addTextArea('content', 'Add post:')
			->setRequired();
		$form->addSubmit('send', 'Public new content');
		$form->onSuccess[] = array($this, 'postFormSucceeded');
		return $form;
	}

	/**
	 * @return UI\Form
	 */
	public function postFormSucceeded($form, $values)
	{
		$values = $this->getValues('classId');
		if ($classId) {
			$class = $this->database->table('class')->get($classId);
			$class = $this->table->update($values);
		} else { $class = $this->database->table('class')->insert($values);
		}
		$this->flashMessage('Content was published', 'success');
		$this->redirect('show', $class->id);
	}

}
