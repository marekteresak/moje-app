<?php

namespace Render\Forms;

use Nette\Forms\Rendering\DefaultFormRenderer,
	Nette\Application\UI;

class BaseFormRenderer extends DefaultFormRenderer
{

	public $wrappers = array(
		'form' => array(
			'container' => 'div class="small-3 large-2 columns"',
			'errors' => TRUE,
		),

		'error' => array(
			'container' => 'div class="alert-message error"',
			'item' => 'p',
		),

		'group' => array(
			'container' => 'fieldset',
			'label' => 'legend',
			'description' => 'p',
		),

		'controls' => array(
			'container' => '',
		),

		'pair' => array(
			'container' => 'div class="row"',
			'.required' => 'required',
			'.optional' => NULL,
			'.odd' => NULL,
		),

		'control' => array(
			'container' => 'div class="large-9 columns medium-9"',
			'radiolist' => 'li',
			'.odd' => NULL,

			'errors' => FALSE,
			'description' => 'small',
			'requiredsuffix' => '',

			'.required' => 'required',
			'.text' => 'text span7',
			'.password' => 'text',
			'.file' => 'text',
			'.submit' => 'button primary',
			'.image' => 'imagebutton',
			'.button' => 'button',
		),

		'label' => array(
			'container' => 'div class="columns medium-3"',
			'suffix' => NULL,
			'requiredsuffix' => '',
		),

		'hidden' => array(
			'container' => 'div',
		),
	);


	public static function factory()
	{
		$form = new UI\Form;
		$form->setRenderer(new BaseFormRenderer);

		return $form;
	}
}
