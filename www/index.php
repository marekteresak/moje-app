<?php
/**
 * Created by PhpStorm.
 * User: Marek
 * Date: 21.5.15
 * Time: 0:26
 */
// Uncomment this line if you must temporarily take down your site for maintenance.
// require '.maintenance.php';

$container = require __DIR__ . '/../system/booter.php';

$container->getService('application')->run();

